package com.qaagility.controller;

public class Count {

    public int division(int num, int den) {
        if (den == 0)
            return Integer.MAX_VALUE;
        else
            return num / den;
    }

}
